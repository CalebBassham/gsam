package main

import (
	"gitlab.com/CalebBassham/gsam/cmd"
)

func main() {
	cmd.Execute()
}
