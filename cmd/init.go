package cmd

import (
	"github.com/spf13/cobra"
	"encoding/json"
	"gitlab.com/CalebBassham/gsam/models"
	"fmt"
	"os"
)

var initCmd = &cobra.Command{
	Use: "init",
	Short: "Create the default config file.",
	Run: func(cmd *cobra.Command, args []string) {
		createConfig()
		createSecretConfig()
	},
}

func createConfig() {
	js, err := json.MarshalIndent(models.DefaultConfig, "", "  ")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	fmt.Println(string(js))

	file, err := os.Create("gsam-config.json")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer file.Close()

	if _, err = file.Write(js); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func createSecretConfig() {
	js, err := json.MarshalIndent(models.DefaultSecretConfig, "", "  ")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	file, err := os.Create("gsam-secret-config.json")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer file.Close()

	if _, err = file.Write(js); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	fmt.Println("Created gsam-secret-config.json")
}

func init() {
	rootCmd.AddCommand(initCmd)
}
