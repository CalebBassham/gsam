package cmd

import (
	"github.com/spf13/cobra"
	"fmt"
	"os"
	"log"
)

var Log = log.New(os.Stdout, "", 0)
var ErrorLog = log.New(os.Stderr, "", 0)

var rootCmd = &cobra.Command{
	Use: "gsam",
	Short: "GSAM manages go serverless applications.",
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Help()
	},
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
