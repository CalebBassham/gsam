package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/CalebBassham/gsam/models"
	"fmt"
	"os"
	"strings"
	"path/filepath"
	"archive/zip"
	"io"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/lambda"
	"io/ioutil"
	"os/exec"
	"github.com/aws/aws-sdk-go/aws/session"
	"encoding/json"
)

var uploadCmd = &cobra.Command{
	Use: "upload",
	Run: handleUploadCmd,
}

func handleUploadCmd(cmd *cobra.Command, args []string) {
	stage, err := cmd.Flags().GetString("stage")
	if err != nil {
		ErrorLog.Printf(`Failed to get flag "stage" because %s`, err)
		return
	}
	stage = strings.ToLower(stage)
	if stage != "all" && stage != "build" && stage != "upload" {
		ErrorLog.Printf("The stage flag must be all, build, or upload.")
		return
	}

	exitOnError, _ := cmd.Flags().GetBool("exitOnError")

	configPath, _ := cmd.Flags().GetString("config")

	config, err := models.ReadConfig(configPath)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	secretConfig, err := models.ReadSecretConfig()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	os.Setenv("AWS_ACCESS_KEY_ID", secretConfig.AWSAccessKey.ID)
	os.Setenv("AWS_SECRET_ACCESS_KEY", secretConfig.AWSAccessKey.Secret)

	uploadAll, _ := cmd.Flags().GetBool("all")
	if len(args) == 0 {
		uploadAll = true
	}

	var functions []models.LambdaFunction

	if uploadAll {
		functions = config.LambdaFunctions
	} else {
		functions = []models.LambdaFunction{}

		for _, arg := range args {
			for _, function := range config.LambdaFunctions {
				if strings.ToLower(arg) == strings.ToLower(function.Name) {
					functions = append(functions, function)
				}
			}
		}
	}

	shouldClean, err := cmd.Flags().GetBool("clean")
	if err != nil {
		ErrorLog.Printf(`Failed to get flag "clean" because %s using default clean=true`, err)
		shouldClean = true
	}

	verbose, _ := cmd.Flags().GetBool("verbose")
	runGoGet, _ := cmd.Flags().GetBool("get")

	switch stage {
	case "all":
		AllStages(config, secretConfig, configPath, functions, exitOnError, verbose, runGoGet)
	case "build":
		BuildOnly(functions, exitOnError, verbose, runGoGet)
	case "upload":
		UploadOnly(functions, config, secretConfig, configPath, exitOnError)
	}

	if shouldClean {
		clean()
	}
}

type BuildResult struct {
	LambdaFunc         *models.LambdaFunction
	GetCommandOutput   string
	BuildCommandOutput string
	Error              error
}

func Build(doneChan chan BuildResult, runGoGet bool, lambdaFunc *models.LambdaFunction) {
	clean()
	Log.Print("DID clean")

	lambdaPath, err := filepath.Abs(lambdaFunc.Path)
	if err != nil {
		doneChan <- BuildResult{Error: err, LambdaFunc: lambdaFunc}
		return
	}

	var getOutput string
	// go get
	if runGoGet {
		Log.Print("SHOULD get")
		cmd := exec.Command("go", "get", "-v")

		cmd.Dir = lambdaPath

		out, err := cmd.CombinedOutput()
		if err != nil {
			doneChan <- BuildResult{Error: err, LambdaFunc: lambdaFunc}
			return
		}
		getOutput = string(out)
		Log.Print("DID go get")
	}

	// go build
	outputPath, err := filepath.Abs(filepath.Join(BuildDirectoryPath(), lambdaFunc.Name))
	if err != nil {
		doneChan <- BuildResult{Error: err, LambdaFunc: lambdaFunc, GetCommandOutput: getOutput}
		return
	}

	cmd := exec.Command("go", "build", "-race", "-v", "-o", outputPath)
	cmd.Dir = lambdaPath

	out, err := cmd.CombinedOutput()
	if err != nil {
		doneChan <- BuildResult{Error: err, LambdaFunc: lambdaFunc, GetCommandOutput: getOutput}
		return
	}

	Log.Print("DID go build")

	doneChan <- BuildResult{LambdaFunc: lambdaFunc, BuildCommandOutput: string(out), GetCommandOutput: getOutput}
}

type ZipResult struct {
	LambdaFunction *models.LambdaFunction
	Error          error
}

func Zip(doneChan chan ZipResult, lambdaFunc *models.LambdaFunction) {
	zipFile, err := os.Create(filepath.Join(BuildDirectoryPath(), lambdaFunc.Name+".zip"))
	if err != nil {
		doneChan <- ZipResult{Error: err, LambdaFunction: lambdaFunc}
		return
	}
	zipWriter := zip.NewWriter(zipFile)

	path := filepath.Join(BuildDirectoryPath(), lambdaFunc.Name)
	binaryFile, err := os.Open(path)
	if err != nil {
		doneChan <- ZipResult{Error: err, LambdaFunction: lambdaFunc}
		return
	}

	binaryFileInfo, err := binaryFile.Stat()
	if err != nil {
		doneChan <- ZipResult{Error: err, LambdaFunction: lambdaFunc}
		return
	}

	binaryFileHeader, err := zip.FileInfoHeader(binaryFileInfo)
	if err != nil {
		doneChan <- ZipResult{Error: err, LambdaFunction: lambdaFunc}
		return
	}

	writer, err := zipWriter.CreateHeader(binaryFileHeader)
	if err != nil {
		doneChan <- ZipResult{Error: err, LambdaFunction: lambdaFunc}
		return
	}

	_, err = io.Copy(writer, binaryFile)
	if err != nil {
		doneChan <- ZipResult{Error: err, LambdaFunction: lambdaFunc}
		return
	}

	binaryFile.Close()
	zipWriter.Close()
	zipFile.Close()

	doneChan <- ZipResult{LambdaFunction: lambdaFunc}
}

type CreateResult struct {
	LambdaFunc     *models.LambdaFunction
	AlreadyCreated bool
	Region         string
	Error          error
}

func Create(doneChan chan CreateResult, lambdaSvc *lambda.Lambda, runtime string, secretConfig *models.SecretConfig, region string, lambdaFunc *models.LambdaFunction) {
	if lambdaFunc.Created {
		doneChan <- CreateResult{LambdaFunc: lambdaFunc, AlreadyCreated: true, Region: region}
		return
	}

	var role *models.Arn
	if lambdaFunc.RoleArnName == "" {
		arn, err := secretConfig.DefaultRoleArn()
		if err != nil {
			doneChan <- CreateResult{Error: err, LambdaFunc: lambdaFunc}
			return
		}

		role = arn
	} else {
		arn, err := secretConfig.GetArn(lambdaFunc.RoleArnName)
		if err != nil {
			doneChan <- CreateResult{Error: err, LambdaFunc: lambdaFunc}
			return
		}

		role = arn
	}

	data, err := ioutil.ReadFile(filepath.Join(BuildDirectoryPath(), lambdaFunc.Name+".zip"))
	if err != nil {
		doneChan <- CreateResult{Error: err, LambdaFunc: lambdaFunc}
		return
	}

	_, err = lambdaSvc.CreateFunction(&lambda.CreateFunctionInput{
		FunctionName: aws.String(lambdaFunc.Name),
		Handler:      aws.String(lambdaFunc.Name),
		Role:         aws.String(role.Arn),
		Publish:      aws.Bool(true),
		Runtime:      aws.String(runtime),
		Code: &lambda.FunctionCode{
			ZipFile: data,
		},
	})
	if err != nil {
		lambdaFunc.Created = true
		if strings.HasPrefix(err.Error(), "ResourceConflictException: Function already exist") {
			doneChan <- CreateResult{LambdaFunc: lambdaFunc, AlreadyCreated: true, Region: region}
			return
		} else {
			doneChan <- CreateResult{Error: err, LambdaFunc: lambdaFunc}
			return
		}
	}

	doneChan <- CreateResult{LambdaFunc: lambdaFunc, Region: region}
}

type UpdateResult struct {
	LambdaFunctionName string
	Region             string
	Error              error
}

func Update(doneChan chan UpdateResult, lambdaSvc *lambda.Lambda, region string, lambdaFuncName string) {
	data, err := ioutil.ReadFile(filepath.Join(BuildDirectoryPath(), lambdaFuncName+".zip"))
	if err != nil {
		doneChan <- UpdateResult{Error: err, LambdaFunctionName: lambdaFuncName}
		return
	}

	_, err = lambdaSvc.UpdateFunctionCode(&lambda.UpdateFunctionCodeInput{
		FunctionName: aws.String(lambdaFuncName),
		Publish:      aws.Bool(true),
		ZipFile:      data,
	})
	if err != nil {
		doneChan <- UpdateResult{Error: err, LambdaFunctionName: lambdaFuncName}
		return
	}

	doneChan <- UpdateResult{LambdaFunctionName: lambdaFuncName, Region: region}
}

func BuildDirectoryPath() string {
	path, _ := filepath.Abs("./gsam-build/")
	return path
}

func clean() {
	os.RemoveAll(BuildDirectoryPath())
}

func AllStages(config *models.Config, secretConfig *models.SecretConfig, configPath string, functions []models.LambdaFunction, exitOnError bool, verbose bool, runGoGet bool) {
	buildDoneChans := make([]chan BuildResult, len(functions))

	// build
	for i := range functions {
		lambdaFunc := &functions[i]
		doneChan := make(chan BuildResult)
		buildDoneChans[i] = doneChan
		go Build(doneChan, runGoGet, lambdaFunc)
	}

	var zipDoneChans []chan ZipResult

	// zip
	for _, doneChan := range buildDoneChans {
		buildResult := <-doneChan

		if buildResult.Error == nil {
			doneChan := make(chan ZipResult)
			zipDoneChans = append(zipDoneChans, doneChan)
			go Zip(doneChan, buildResult.LambdaFunc)
		} else {
			ErrorLog.Printf(`Failed to build lambda function "%s" because %s`, buildResult.LambdaFunc.Name, buildResult.Error)
			if verbose && buildResult.BuildCommandOutput != "" {
				Log.Printf(`BUILD COMMAND OUTPUT: %s`, buildResult.BuildCommandOutput)
			}
			if exitOnError {
				os.Exit(1)
			}
		}
	}

	var createDoneChans []chan CreateResult

	// create
	for _, doneChan := range zipDoneChans {
		zipResult := <-doneChan

		if zipResult.Error == nil {
			for _, region := range config.Regions {
				doneChan := make(chan CreateResult)

				createDoneChans = append(createDoneChans, doneChan)

				sess, _ := session.NewSession(&aws.Config{
					Region: aws.String(region),
				})

				lambdaSvc := lambda.New(sess)

				go Create(doneChan, lambdaSvc, config.Runtime, secretConfig, region, zipResult.LambdaFunction)
			}
		} else {
			ErrorLog.Printf(`Failed to zip lambda function "%s" because %s`, zipResult.LambdaFunction.Name, zipResult.Error)
			if exitOnError {
				os.Exit(1)
			}
		}
	}

	var updateDoneChans []chan UpdateResult

	// update
	for _, doneChan := range createDoneChans {
		createResult := <-doneChan

		if createResult.Error == nil {
			if createResult.AlreadyCreated {
				doneChan := make(chan UpdateResult)
				updateDoneChans = append(updateDoneChans, doneChan)

				sess, _ := session.NewSession(&aws.Config{
					Region: aws.String(createResult.Region),
				})

				lambdaSvc := lambda.New(sess)

				go Update(doneChan, lambdaSvc, createResult.Region, createResult.LambdaFunc.Name)
			} else {
				Log.Printf(`Created lambda function "%s" in region "%s"`, createResult.LambdaFunc.Name, createResult.Region)
			}
		} else {
			ErrorLog.Printf(`Failed to create lambda function "%s" in region "%s" because %s`, createResult.LambdaFunc.Name, createResult.Region, createResult.Error)
			if exitOnError {
				os.Exit(1)
			}
		}
	}

	for _, doneChan := range updateDoneChans {
		updateResult := <-doneChan

		if updateResult.Error != nil {
			ErrorLog.Printf(`Failed to update lambda function "%s" in region "%s" because %s`, updateResult.LambdaFunctionName, updateResult.Region, updateResult.Error)
			if exitOnError {
				os.Exit(1)
			}
		} else {
			Log.Printf(`Updated lambda function "%s" in region "%s"`, updateResult.LambdaFunctionName, updateResult.Region)
		}
	}

	data, _ := json.MarshalIndent(config, "", "  ")
	ioutil.WriteFile(configPath, data, 755)
}

func BuildOnly(functions []models.LambdaFunction, exitOnError bool, verbose bool, runGoGet bool) {
	buildDoneChans := make([]chan BuildResult, len(functions))

	// build
	for i := range functions {
		lambdaFunc := &functions[i]
		doneChan := make(chan BuildResult)
		buildDoneChans[i] = doneChan
		go Build(doneChan, runGoGet, lambdaFunc)
	}

	var zipDoneChans []chan ZipResult

	// zip
	for _, doneChan := range buildDoneChans {
		buildResult := <-doneChan

		if buildResult.Error == nil {
			doneChan := make(chan ZipResult)
			zipDoneChans = append(zipDoneChans, doneChan)
			go Zip(doneChan, buildResult.LambdaFunc)
		} else {
			ErrorLog.Printf(`Failed to build lambda function "%s" because %s`, buildResult.LambdaFunc.Name, buildResult.Error)
			if verbose && buildResult.BuildCommandOutput != "" {
				Log.Printf(`BUILD COMMAND OUTPUT: %s`, buildResult.BuildCommandOutput)
			}
			if verbose && buildResult.GetCommandOutput != "" {
				Log.Printf("GET COMMAND OUTPUT: %s", buildResult.GetCommandOutput)
			}
			if exitOnError {
				os.Exit(1)
			}
		}
	}

	for _, doneChan := range zipDoneChans {
		zipResult := <-doneChan

		if zipResult.Error != nil {
			ErrorLog.Printf(`Failed to zip lambda function "%s" because %s`, zipResult.LambdaFunction.Name, zipResult.Error)
			if exitOnError {
				os.Exit(1)
			}
		}
	}
}

func UploadOnly(functions []models.LambdaFunction, config *models.Config, secretConfig *models.SecretConfig, configPath string, exitOnError bool) {
	var createDoneChans []chan CreateResult

	// create
	for _, region := range config.Regions {
		sess, _ := session.NewSession(&aws.Config{
			Region: aws.String(region),
		})

		lambdaSvc := lambda.New(sess)

		for i := range functions {
			lambdaFunc := &functions[i]
			doneChan := make(chan CreateResult)
			createDoneChans = append(createDoneChans, doneChan)
			go Create(doneChan, lambdaSvc, config.Runtime, secretConfig, region, lambdaFunc)
		}
	}

	var updateDoneChans []chan UpdateResult

	// update
	for _, doneChan := range createDoneChans {
		createResult := <-doneChan

		if createResult.Error == nil {
			if createResult.AlreadyCreated {
				doneChan := make(chan UpdateResult)
				updateDoneChans = append(updateDoneChans, doneChan)

				sess, _ := session.NewSession(&aws.Config{
					Region: aws.String(createResult.Region),
				})

				lambdaSvc := lambda.New(sess)

				go Update(doneChan, lambdaSvc, createResult.Region, createResult.LambdaFunc.Name)
			} else {
				Log.Printf(`Created lambda function "%s" in region "%s"`, createResult.LambdaFunc.Name, createResult.Region)
			}
		} else {
			ErrorLog.Printf(`Failed to create lambda function "%s" in region "%s" because %s`, createResult.LambdaFunc.Name, createResult.Region, createResult.Error)
			if exitOnError {
				os.Exit(1)
			}
		}
	}

	for _, doneChan := range updateDoneChans {
		updateResult := <-doneChan

		if updateResult.Error != nil {
			ErrorLog.Printf(`Failed to update lambda function "%s" in region "%s" because %s`, updateResult.LambdaFunctionName, updateResult.Region, updateResult.Error)
			if exitOnError {
				os.Exit(1)
			}
		} else {
			Log.Printf(`Updated lambda function "%s" in region "%s"`, updateResult.LambdaFunctionName, updateResult.Region)
		}
	}

	data, _ := json.MarshalIndent(config, "", "  ")
	ioutil.WriteFile(configPath, data, 755)
}

func init() {
	rootCmd.AddCommand(uploadCmd)

	defaultConfigPath, err := models.DefaultConfigPath()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	uploadCmd.Flags().StringP("config", "c", defaultConfigPath, "Specify an alternative path to config file.")
	uploadCmd.Flags().Bool("all", false, "Upload all declared lambda functions.")
	uploadCmd.Flags().Bool("clean", true, "Specify if a Build directory should be cleaned before and after building.")
	uploadCmd.Flags().String("stage", "all", "Specify what stage to run. Valid options are all, build, and upload.")
	uploadCmd.Flags().Bool("exitOnError", false, "Exits with error code 1 when there is an error with building or uploading a lambda function. Useful for CI & CD.")
	uploadCmd.Flags().BoolP("verbose", "v", false, "Show verbose output.")
	uploadCmd.Flags().Bool("get", false, "Run go get before building.")
}
