package cmd

import (
	"github.com/spf13/cobra"
	"os"
	"gitlab.com/CalebBassham/gsam/models"
	"fmt"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/apigateway"
	"strings"
)

// gsam deploy <stage> [description]
var deployCmd = &cobra.Command{
	Use: "deploy",
	Run: handleDeployCmd,
}

func handleDeployCmd(cmd *cobra.Command, args []string) {
	if len(args) < 1 {
		fmt.Println("You must specify a stage to deploy to.")
		return
	}

	configPath, _ := cmd.Flags().GetString("config")
	config, err := models.ReadConfig(configPath)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	secretConfig, err := models.ReadSecretConfig()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	os.Setenv("AWS_ACCESS_KEY_ID", secretConfig.AWSAccessKey.ID)
	os.Setenv("AWS_SECRET_ACCESS_KEY", secretConfig.AWSAccessKey.Secret)

	stage := args[0]

	var description string
	if len(args) > 1 {
		description = strings.Join(args[1:], " ")
	}

	doneChans := make([]chan struct{}, len(config.Regions))

	for i, region := range config.Regions {
		doneChan := make(chan struct{})
		doneChans[i] = doneChan

		go deploy(doneChan, region, secretConfig.ApiID, stage, description)
	}

	for _, doneChan := range doneChans {
		<-doneChan
	}

}

func deploy(done chan struct{}, region string, restApiId string, stage string, description string)  {
	sess, _ := session.NewSession(&aws.Config{
		Region: aws.String(region),
	})

	gateway:= apigateway.New(sess)

	_, err := gateway.CreateDeployment(&apigateway.CreateDeploymentInput{
		RestApiId: aws.String(restApiId),
		Description: aws.String(description),
		StageName: aws.String(stage),
	})

	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to deploy stage '%s' in region '%s'", stage, region)
		fmt.Fprint(os.Stderr, err)
	}

	fmt.Println(fmt.Sprintf("Deployed to stage '%s' in region '%s'", stage, region))
	done<- struct{}{}
	close(done)
}

func init() {
	rootCmd.AddCommand(deployCmd)

	defaultConfigPath, err := models.DefaultConfigPath()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	deployCmd.Flags().StringP("config", "c", defaultConfigPath, "Specify an alternative path to config file.")
}
