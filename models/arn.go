package models

type Arn struct {
	Name string `json:"name"`
	Arn  string `json:"arn"`
}
