package models

import (
	"path/filepath"
	"io/ioutil"
	"encoding/json"
)

type Config struct {
	Runtime         string           `json:"runtime"`
	Regions         []string         `json:"regions"`
	LambdaFunctions []LambdaFunction `json:"lambdaFunctions"`
}

var DefaultConfig = &Config{
	Runtime:        "go1.x",
	Regions:        []string{"us-east-2"},
	LambdaFunctions: []LambdaFunction{
		{Name: "getRelationships", Path: "./getRelationships/main.go"},
		{Name: "getRelationship", Path: "./getRelationship/main.go", RoleArnName: "some-other-arn"},
	},
}

func DefaultConfigPath() (string, error) {
	path, err := filepath.Abs("./gsam-config.json")
	if err != nil {
		return "", err
	}

	return path, nil
}

func ReadConfig(path string) (*Config, error) {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}

	config := &Config{}
	if err = json.Unmarshal(data, config); err != nil {
		return nil, err
	}

	return config, nil
}
