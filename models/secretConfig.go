package models

import (
	"path/filepath"
	"os"
	"encoding/json"
	"io/ioutil"
	"fmt"
)

type SecretConfig struct {
	AWSAccessKey AWSAccessKey `json:"awsAccessKey"`
	Arns         []Arn        `json:"arns"`
	ApiID        string       `json:"apiId"`
}

func (config SecretConfig) GetArn(name string) (*Arn, error) {
	for i := range config.Arns {
		arn := &config.Arns[i]
		if arn.Name == name {
			return arn, nil
		}
	}

	return nil, fmt.Errorf(`there is no arn with the name "%s"`, name)
}

func (config SecretConfig) DefaultRoleArn() (*Arn, error) {
	return config.GetArn("default")
}

var DefaultSecretConfig = &SecretConfig{
	AWSAccessKey: AWSAccessKey{
		ID: "xxxxxxxxxxxxxxxxxxxx",
		Secret: "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
	},
	Arns: []Arn{
		{Name: "default", Arn: "arn:<partition>:<service>::<accountId>:<type>/<resource>"},
	},
	ApiID: "xxxxxxxxxx",
}

func DefaultSecretConfigPath() (string, error) {
	path, err := filepath.Abs("./gsam-secret-config.json")
	if err != nil {
		return "", err
	}

	return path, nil
}

func SecretConfigEnvironmentExists() bool {
	return os.Getenv("GSAM_SECRET_CONFIG") != ""
}


func readSecretConfigFromEnvironment() (*SecretConfig, error) {
	cfgStr := os.Getenv("GSAM_SECRET_CONFIG")
	config := &SecretConfig{}
	if err := json.Unmarshal([]byte(cfgStr), config); err != nil {
		return nil, err
	}
	return config, nil
}

func SecretConfigFileExists() (bool, error) {
	path, err := DefaultSecretConfigPath()
	if err != nil {
		return false, err
	}

	if _, err := os.Stat(path); os.IsNotExist(err) {
		return false, nil
	}

	return true, nil
}

func readSecretConfigFromFile() (*SecretConfig, error) {
	path, err := DefaultSecretConfigPath()
	if err != nil {
		return nil, err
	}

	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}

	config := &SecretConfig{}
	if err := json.Unmarshal(data, config); err != nil {
		return nil, err
	}

	return config, nil
}

func ReadSecretConfig() (*SecretConfig, error) {
	fileExists, err := SecretConfigFileExists()
	if err != nil {
		return nil, err
	}

	if fileExists {
		config, err := readSecretConfigFromFile()
		if err != nil {
			return nil, err
		}
		return config, nil
	}

	if SecretConfigEnvironmentExists() {
		config, err := readSecretConfigFromEnvironment()
		if err != nil {
			return nil, err
		}
		return config, nil
	}

	return nil, fmt.Errorf("secret config file does not exist as file or environment variable")
}