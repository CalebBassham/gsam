package models

type LambdaFunction struct {
	Name        string `json:"name"`
	Path        string `json:"path"`
	RoleArnName string `json:"roleARN,omitempty"`
	Created     bool   `json:"created,omitempty"`
}
