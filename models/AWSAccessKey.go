package models

type AWSAccessKey struct {
	ID     string `json:"id"`
	Secret string `json:"secret"`
}
